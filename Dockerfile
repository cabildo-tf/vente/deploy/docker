ARG DOCKER_VERSION=19

FROM docker:${DOCKER_VERSION}

RUN apk add --no-cache \
        bash-completion=~2

CMD ["bash"]